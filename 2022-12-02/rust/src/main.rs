use std::env;
use std::io::{BufRead, BufReader};
use std::fs::File;
use std::ops::Add;

#[derive(Copy, Clone, Eq, PartialEq)]
enum Round {
    Rock = 1,
    Paper,
    Scissors,
}

#[derive(Debug)]
struct Vec2(u32, u32);

impl Add for Vec2 {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self(self.0 + other.0, self.1 + other.1)
    }
}

fn strategy1(_enemy: Round, strat: char) -> Round {
    match strat {
        'X' => Round::Rock,
        'Y' => Round::Paper,
        'Z' => Round::Scissors,
        _   => panic!("Undefined"),
    }
}

fn strategy2(enemy: Round, strat: char) -> Round {
    match strat {
        'X' => match enemy { Round::Paper => Round::Rock, Round::Rock => Round::Scissors, Round::Scissors => Round::Paper }
        'Y' => enemy,
        'Z' => match enemy { Round::Rock => Round::Paper, Round::Paper => Round::Scissors, Round::Scissors => Round::Rock }
        _   => panic!("Undefined"),
    }
}


fn parse_line<T>(line: &String, decide: T) -> (Round, Round) 
    where T: FnOnce(Round, char) -> Round
{
    let res = line.as_bytes();
    let enemy = match res[0] as char {
        'A' => Round::Rock,
        'B' => Round::Paper,
        'C' => Round::Scissors,
        _   => panic!("Undefined"),
    };
    let yours = decide(enemy, res[2] as char);
    (enemy, yours)
}

fn score(round: (Round, Round)) -> Vec2 {
    let res = 
        match round {
            (Round::Rock, Round::Paper)     => Vec2(0, 6),
            (Round::Rock, Round::Scissors)  => Vec2(6, 0),
            (Round::Paper, Round::Rock)     => Vec2(6, 0),
            (Round::Paper, Round::Scissors) => Vec2(0, 6),
            (Round::Scissors, Round::Paper) => Vec2(6, 0),
            (Round::Scissors, Round::Rock)  => Vec2(0, 6),
            _ => Vec2(3, 3),
        };
    res + Vec2(round.0 as u32, round.1 as u32)
}



fn main() {
    let arg = env::args().nth(1).expect("Requires arg to input file");
    let fd = File::open(arg).expect("Could not open input file");
    let f = BufReader::new(fd);

    let mut lines: Vec<String> = vec![];
    for line in f.lines() {
        lines.push(line.unwrap());
    }

    let input = lines.iter().map(|x| parse_line(x, strategy1)).collect::<Vec<(Round,Round)>>();
    let result = input.iter().fold(Vec2(0,0), |acc, round| acc + score(*round));
    println!("Result: {:?}", result);

    let input = lines.iter().map(|x| parse_line(x, strategy2)).collect::<Vec<(Round,Round)>>();
    let result = input.iter().fold(Vec2(0,0), |acc, round| acc + score(*round));
    println!("Result: {:?}", result);
}
