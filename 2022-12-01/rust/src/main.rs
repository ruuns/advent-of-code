use std::env;
use std::io::{BufRead, BufReader, Error, ErrorKind};
use std::fs::File;
use std::collections::BinaryHeap;
use std::cmp::Reverse;

enum Ev {
    Item(u32),
    NewLine,
}

fn parse_line(line: Result<String, std::io::Error>) -> Result<Ev, std::io::Error> {
    let res = line?;
    if res.is_empty() {
        return Ok(Ev::NewLine)
    }
    let calory = res.parse::<u32>().map_err(|e| Error::new(ErrorKind::Other, e))?;
    Ok(Ev::Item(calory))
}

fn compute_nth_biggest_inventories<'a, T>(ev: T, num: usize) -> Vec<u32>
   where T: Iterator<Item = &'a Ev> 
{
    let mut heap = BinaryHeap::new();
    ev.fold(0, |acc, ev| 
        match ev {
            Ev::NewLine  => {
                heap.push(Reverse(acc));
                while heap.len() > num {
                    heap.pop();
                };
                return 0
            },
            Ev::Item(x)  => acc + x, 
        }
    );
    heap.iter().map(|Reverse(x)| x.clone()).collect()
}

fn main() {
    let arg = env::args().nth(1).expect("Requires arg to input file");
    let fd = File::open(arg).expect("Could not open input file");
    let f = BufReader::new(fd);
    let mut events = f.lines().map(|x| parse_line(x).expect("Error")).collect::<Vec<Ev>>();
    events.push(Ev::NewLine);

    let best1 = compute_nth_biggest_inventories(events.iter(), 1);
    let best3 = compute_nth_biggest_inventories(events.iter(), 3);

    print!("Result: {:?}", best1.iter().sum::<u32>());
    print!("Result: {:?}", best3.iter().sum::<u32>())
}
