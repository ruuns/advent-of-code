#include <stdexcept>
#include <fstream>
#include <vector>
#include <cstdio>
#include <cctype>

struct Op {
	int from;
	int to;
	int items;
};

using Stacks = std::vector<std::vector<char>>;
using Operations = std::vector<Op>;

void parseStacksFromInput(std::ifstream& input, Stacks* stacks) {
	std::vector<std::string> lines;
	
	std::string line;
	while (!input.eof()) {
		std::getline(input, line);

		if (line.empty())
			break;

		printf("Line: %s\n", line.c_str());

		lines.push_back(line);
	}

	for (auto it = lines.rbegin(); it != lines.rend(); ++it) {
		if (stacks->size() == 0) {
			stacks->resize((it->size() + 1) / 4);
			continue;
		}

		unsigned int i = 0;
		for (auto& stack : *stacks) {
			char item = (*it)[i * 4 + 1];
			if (!std::isspace(item))
				stack.push_back(item);
			++i;
		}
	}
}

void parseOperationsFromInput(std::ifstream& input, Operations* ops) {
	std::string line;
	while (!input.eof()) {
		std::getline(input, line);

		Op op;
		int args = std::sscanf(line.c_str(), "move %i from %i to %i", &op.items, &op.from, &op.to);
		if (args < 3)
			continue;

		ops->push_back(op);
	}
}


int main(int argc, char** argv) {
	if (argc <= 1) {
		fprintf(stderr, "No input file given\n");
		return 0;
	}

	std::ifstream input(argv[1]);
	//input.exceptions (std::ifstream::failbit | std::ifstream::badbit);

	Stacks stacks;
	Operations ops;

	parseStacksFromInput(input, &stacks);
	parseOperationsFromInput(input, &ops);

	bool crateMover9001 = true;

	for (const auto& op : ops) {
		auto &from = stacks[op.from - 1];
		auto &to = stacks[op.to - 1];

		for(unsigned int i = 0; !crateMover9001 && i < op.items; ++i) {
			char top = from.back();
			from.pop_back();
			to.push_back(top);
		}

		if (crateMover9001) {
			to.insert(to.end(), from.end() - op.items, from.end());
			from.resize(from.size() - op.items);
		}
	}

	std::string codeword;
	for (const auto& s : stacks) {
		codeword.push_back(s.back());
	}
	printf("%s\n", codeword.c_str());
}
