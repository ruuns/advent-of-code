use std::env;
use std::io::{BufRead, BufReader};
use std::fs::File;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
struct Range(u32, u32);

impl FromStr for Range {
    type Err = <u32 as FromStr>::Err;

    fn from_str(txt: &str) -> Result<Self, Self::Err> {
        let pos = txt.find('-').unwrap();
        let (beg, end) = txt.split_at(pos);
        let b = beg.parse::<u32>()?;
        let e = end[1..].parse::<u32>()?;
        return Ok(Range(b,e))
    }
}

fn parse_line(line: &str) -> (Range, Range) {
    let pos = line.find(',').unwrap();
    let (fstStr, sndStr) = line.split_at(pos);
    let fst = Range::from_str(fstStr).unwrap();
    let snd = Range::from_str(&sndStr[1..]).unwrap();
    return (fst, snd)
}

fn is_fully_contained(Range(a_b, a_e): Range, Range(b_b, b_e): Range) -> bool {
    return a_b <= b_b && b_e <= a_e
}

/*  w -------- x
 *  y -------- z
 */

fn is_overlap(Range(w, x): Range, Range(y, z): Range) -> bool {
    return x >= y && w <= z
}

fn main() {
    let arg = env::args().nth(1).expect("Requires arg to input file");
    let fd = File::open(arg).expect("Could not open input file");
    let f = BufReader::new(fd);
    let lines = f.lines().map(|str| str.unwrap()).collect::<Vec<String>>();

    let fc = lines.iter()
        .map(|x| parse_line(x.as_str()))
        .fold(0, |acc, (a,b)| acc + if is_fully_contained(a,b) || is_fully_contained(b,a) { 1 } else { 0 });

    let ov = lines.iter()
        .map(|x| parse_line(x.as_str()))
        .fold(0, |acc, (a,b)| acc + if is_overlap(a,b) { 1 } else { 0 });


    println!("Res: {:?} {:?}", fc, ov);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_str() {
        assert_eq!(Range::from_str("31-32"), Ok(Range(31, 32)));
    }

    #[test]
    fn test_parse_line() {
        assert_eq!(parse_line("18-18,19-68"), (Range(18,18), Range(19,68)));
    }
}
