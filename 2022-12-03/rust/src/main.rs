use std::env;
use std::io::{BufRead, BufReader};
use std::fs::File;

fn compute_type_mask(cmp: &str) -> u64 {
    let mut mask = 0u64;
    for ch in cmp.chars() {
        if ch.is_lowercase() {
            mask |= 1 << (ch as u32 - 'a' as u32 + 1u32);
        } else {
            mask |= 1 << (ch as u32 - 'A' as u32 + 27u32);
        }
    }
    return mask
}

fn compute_priority(type_mask: u64) -> u32 {
    let mut sum = 0u32;
    let mut mask = type_mask;
    let mut pos = 0;
    while mask > 0 {
        if mask & 1 > 0 {
            sum += pos as u32;
        }
        mask >>= 1;
        pos += 1;
    }
    return sum
}

fn compute(pkg: &str) -> u32 {
    let half = pkg.len() / 2;
    let (fst, snd) = pkg.split_at(half);
    let mask1 = compute_type_mask(fst);
    let mask2 = compute_type_mask(snd);
    return compute_priority(mask1 & mask2);
}

fn compute_group(group: &[String]) -> u32 {
    let mask1 = compute_type_mask(group[0].as_str());
    let mask2 = compute_type_mask(group[1].as_str());
    let mask3 = compute_type_mask(group[2].as_str());
    return compute_priority(mask1 & mask2 & mask3);
}


fn main() {
    let arg = env::args().nth(1).expect("Requires arg to input file");
    let fd = File::open(arg).expect("Could not open input file");
    let f = BufReader::new(fd);
    let lines = f.lines().map(|str| str.unwrap()).collect::<Vec<String>>();

    let res = lines.iter().fold(0, |acc, string| acc + compute(string.as_str()));
    println!("Result: {:?}", res);

    let mut sum = 0u32;
    for i in 0..lines.len() {
        if i % 3 > 0 {
            continue
        }
        let group = &lines[i..i + 3];
        sum += compute_group(group);
    }
    println!("Group: {:?}", sum)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_type_masks() {
        assert_eq!(compute_type_mask("a"), 1u64 << 1);
        assert_eq!(compute_type_mask("p"), 1u64 << 16);
        assert_eq!(compute_type_mask("A"), 1u64 << 27);
        assert_eq!(compute_type_mask("L"), 1u64 << 38);
    }

    #[test]
    fn test_single_priorities() {
        assert_eq!(compute("vJrwpWtwJgWrhcsFMMfFFhFp"), 16);
        assert_eq!(compute("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"), 38);
        assert_eq!(compute("PmmdzqPrVvPwwTWBwg"), 42);
        assert_eq!(compute("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"), 22);
        assert_eq!(compute("ttgJtRGJQctTZtZT"), 20);
        assert_eq!(compute("CrZsJsPPZsGzwwsLwLmpwMDw"), 19);
    }
}
