module Main where

import System.Environment
import qualified System.IO as IO
import qualified Data.Set as Set
import qualified Data.List as List

detect' :: Int -> Int -> String -> Int
detect' l n txt = 
  if Set.size (Set.fromList (List.take l txt)) < l
    then detect' l (n + 1) (List.tail txt)
    else n

detectStart :: String -> Int 
detectStart txt = detect' 4 4 txt

detectMsg :: String -> Int
detectMsg txt = detect' 14 14 txt

main :: IO ()
main = do 
  args <- fmap head getArgs
  code <- IO.readFile args
  let test = "bvwbjplbgvbhsrlpgdmjqwftvncz"
  let test = "mjqjpqm gbljsphdztnvjfqwrcgsmlb"
  let test = code
  let chrs = detectStart test
  let chrsMsg = detectMsg test
  putStrLn ("Chars read: " ++ show chrs)
  putStrLn ("Msg read:   " ++ show chrsMsg)

