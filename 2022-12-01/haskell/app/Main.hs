module Main where

import System.Environment
import qualified Data.Heap as Heap
import qualified Data.List as List

data Ev = Item Int | NewLine

parseLine :: String -> Ev
parseLine ev
  | null ev   = NewLine
  | otherwise = Item (read ev :: Int)


computeNthBiggestInventories :: [Ev] -> Int -> [Int]
computeNthBiggestInventories [] num = []
computeNthBiggestInventories events num = 
  (Heap.toList . (\x -> Heap.drop (Heap.size x - num) x) . snd . List.foldl' compute (0, initHeap)) events
  where initHeap :: Heap.MinHeap Int
        initHeap = Heap.empty

        compute :: (Int, Heap.MinHeap Int) -> Ev -> (Int, Heap.MinHeap Int)
        compute (acc, heap) NewLine = (0, (Heap.insert acc heap))
        compute (acc, heap) (Item x) = (acc + x, heap)


main :: IO ()
main = do
  args <- fmap head getArgs
  content <- readFile args
  let events = ((map parseLine . lines) content) ++ [NewLine]
  let best1 = computeNthBiggestInventories events 1
  let best3 = computeNthBiggestInventories events 3
  putStrLn ("Best1: " ++ show (sum best1))
  putStrLn ("Best3: " ++ show (sum best3))
